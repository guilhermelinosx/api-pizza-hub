# <div align="center"> API Pizza Hub </div>

</br>

<div align="center">
<p>🚧 It is in Development 🚧</p>
</br>

<p>
This API project was developed to be used in the creation of menus and customer service, using Web App to create menus and order completion and Mobile App to request orders.
</p>
</div>

</br>

## Technologies used in the project

- Typescript
- ExpressJs
- Prisma
- PostgreSQL


## How to run the project

- Clone this repository

```shell
git clone https://github.com/guilhermelinosx/api-pizza-hub.git
```

</br>

- Create a PostgreSQL Container using Docker

```shell
docker container create --name api-pizza-hub -e POSTGRES_PASSWORD=admin -p 5432:5432 postgres
```

</br>

- Start the Container

```shell
docker container start  api-pizza-hub
```

</br>

- Stop the Container

```shell
docker container stop  api-pizza-hub
```

</br>

- Start the Application in Development

```shell
yarn dev
```

</br>

- To stop the Application click CTRL+C in your terminal
