declare namespace Express {
  export interface Request {
    user: string | object
  }
  export interface Response {
    user: string | object
  }
}
