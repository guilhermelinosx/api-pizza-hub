import { Request, Response } from 'express'
import { DetailUserService } from '../services/DetailUserService'
import { ForgotPasswordService } from '../services/ForgotPasswordService'
import { ResetPasswordService } from '../services/ResetPasswordService'
import { SignInUserService } from '../services/SignInUserService'
import { SignUpUserService } from '../services/SignUpUserService'

export class UserController {
  public async signup(req: Request, res: Response): Promise<Response> {
    try {
      const { name, email, password } = req.body
      const signUpUserService = new SignUpUserService()
      const user = await signUpUserService.execute({ name, email, password })
      return res.status(201).json(user)
    } catch (err) {
      return res.status(400).json(err)
    }
  }

  public async signin(req: Request, res: Response): Promise<Response> {
    try {
      const { email, password } = req.body
      const signInUserService = new SignInUserService()
      const user = await signInUserService.execute({ email, password })
      return res.status(200).json(user)
    } catch (err) {
      return res.status(400).json(err)
    }
  }

  public async forgotPassword(req: Request, res: Response): Promise<Response> {
    try {
      const { email } = req.body
      const forgotPasswordService = new ForgotPasswordService()
      const token = await forgotPasswordService.execute({ email })
      return res.status(200).json(token.token)
    } catch (err) {
      return res.status(400).json(err)
    }
  }

  public async resetPassword(req: Request, res: Response): Promise<Response> {
    try {
      const { token, password } = req.body
      const resetPasswordService = new ResetPasswordService()
      await resetPasswordService.execute({ token, password })
      return res.status(200).json({})
    } catch (err) {
      return res.status(400).json(err)
    }
  }

  public async detail(req: Request, res: Response): Promise<Response> {
    try {
      const { user_id } = req.body
      const detailUserService = new DetailUserService()
      const user = await detailUserService.execute(user_id)
      return res.status(200).json(user)
    } catch (err) {
      return res.status(400).json(err)
    }
  }
}
