import { celebrate, Joi, Segments } from 'celebrate'
import { Router } from 'express'
import { isAuthenticated } from '../../../shared/utils/middlewares/isAuthenticated'
import { UserController } from '../controllers/user.controller'

export const UserRouter = Router()
const userController = new UserController()

UserRouter.post(
  '/signup',
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required(),
      email: Joi.string().email().required(),
      password: Joi.string().required()
    }
  }),
  userController.signup
)

UserRouter.post(
  '/signin',
  celebrate({
    [Segments.BODY]: {
      email: Joi.string().email().required(),
      password: Joi.string().required()
    }
  }),
  userController.signin
)

UserRouter.post(
  '/forgotpassword',
  celebrate({
    [Segments.BODY]: {
      email: Joi.string().email().required()
    }
  }),
  userController.forgotPassword
)

UserRouter.post(
  '/resetpassword',
  celebrate({
    [Segments.BODY]: {
      token: Joi.string().uuid().required(),
      password: Joi.string().required()
    }
  }),
  userController.resetPassword
)

UserRouter.get(
  '/detail',
  isAuthenticated,
  celebrate({
    [Segments.BODY]: {
      user_id: Joi.string().uuid().required()
    }
  }),
  userController.detail
)
