import { PrismaORM } from '../../../shared/database'

export class DetailUserService {
  constructor(private readonly prismaorm = PrismaORM) {}

  async execute(user_id: string) {
    const user = await this.prismaorm.user.findFirst({
      where: {
        id: user_id
      },
      select: {
        id: true,
        name: true,
        email: true
      }
    })
    return user
  }
}
