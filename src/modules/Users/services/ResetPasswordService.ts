import { isAfter, addHours } from 'date-fns'
import { hash } from 'bcryptjs'
import { PrismaORM } from '../../../shared/database'
import { InternalAppError } from '../../../shared/utils/errors/internalAppError'

interface IRequest {
  token: string
  password: string
}

export class ResetPasswordService {
  constructor(private readonly prismaorm = PrismaORM) {}

  public async execute({ token, password }: IRequest): Promise<void> {
    const userToken = await this.prismaorm.userToken.findFirst({
      where: {
        token
      }
    })

    if (userToken === null) {
      throw new InternalAppError('User token does not exists.')
    }

    const user = await this.prismaorm.user.findUnique({
      where: {
        id: userToken.user_id
      }
    })

    if (!user) {
      throw new InternalAppError('User does not exists.')
    }

    const tokenCreatedAt = userToken.createdAt

    const compareDate = addHours(tokenCreatedAt, 2)

    if (isAfter(Date.now(), compareDate)) {
      throw new Error('Token expired.')
    }

    user.password = await hash(password, 8)

    await this.prismaorm.user.update({
      where: {
        id: user.id
      },
      data: {
        password: user.password
      }
    })
  }
}
