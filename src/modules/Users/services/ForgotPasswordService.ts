import { PrismaORM } from '../../../shared/database'
import { InternalAppError } from '../../../shared/utils/errors/internalAppError'

interface IRequest {
  email: string
}

interface IResponse {
  token: string
}

export class ForgotPasswordService {
  constructor(private readonly prismaorm = PrismaORM) {}

  public async execute({ email }: IRequest): Promise<IResponse> {
    const user = await this.prismaorm.user.findFirst({
      where: {
        email
      }
    })

    if (user === null) {
      throw new InternalAppError('User does not exists.')
    }

    const { token } = await this.prismaorm.userToken.create({
      data: {
        user_id: user.id
      }
    })

    return {
      token
    }
  }
}
