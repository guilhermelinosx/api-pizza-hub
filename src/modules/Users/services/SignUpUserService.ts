import { hash } from 'bcryptjs'
import { PrismaORM } from '../../../shared/database'
import { InternalAppError } from '../../../shared/utils/errors/internalAppError'

import { IUser } from '../models/IUser'

interface IRequest {
  name: string
  email: string
  password: string
}

export class SignUpUserService {
  constructor(private readonly prismaorm = PrismaORM) {}

  public async execute({ name, email, password }: IRequest): Promise<IUser> {
    const userCheck = await this.prismaorm.user.findFirst({
      where: {
        email
      }
    })

    if (userCheck !== null) {
      throw new InternalAppError('Email address already used.')
    }

    const user = await this.prismaorm.user.create({
      data: {
        name,
        email,
        password: await hash(password, 8)
      }
    })

    return user
  }
}
