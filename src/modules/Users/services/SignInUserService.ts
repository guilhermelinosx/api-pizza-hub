import { compare } from 'bcryptjs'
import { sign } from 'jsonwebtoken'
import { PrismaORM } from '../../../shared/database'
import { InternalAppError } from '../../../shared/utils/errors/internalAppError'

import { IUser } from '../models/IUser'

interface IRequest {
  email: string
  password: string
}

interface IResponse {
  user: IUser
  token: string
}

export class SignInUserService {
  constructor(private readonly prismaorm = PrismaORM) {}

  public async execute({ email, password }: IRequest): Promise<IResponse> {
    const user = await this.prismaorm.user.findFirst({
      where: {
        email
      }
    })

    if (user === null) {
      throw new InternalAppError('User not found.')
    }

    const CheckUserPassword = await compare(password, user.password)

    if (CheckUserPassword === false) {
      throw new InternalAppError('Incorrect email/password combination.')
    }

    const token = sign({}, process.env.JWT_SECRET as string, {
      subject: user.id,
      expiresIn: '1d'
    })

    return {
      user,
      token
    }
  }
}
