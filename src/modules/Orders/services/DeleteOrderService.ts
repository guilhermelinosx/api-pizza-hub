import { PrismaORM } from '../../../shared/database'
import { InternalAppError } from '../../../shared/utils/errors/internalAppError'

interface IRequest {
  id: string
}

export class DeleteOrderService {
  constructor(private readonly prismaorm = PrismaORM) {}

  public async execute({ id }: IRequest): Promise<void> {
    const orderCheck = await this.prismaorm.order.findUnique({
      where: { id }
    })

    if (orderCheck === null) {
      throw new InternalAppError('Order not found')
    }

    await this.prismaorm.order.delete({
      where: { id }
    })
  }
}
