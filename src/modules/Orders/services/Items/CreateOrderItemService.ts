import { PrismaORM } from '../../../../shared/database'
import { InternalAppError } from '../../../../shared/utils/errors/internalAppError'
import { IOrderItem } from '../../models/IOrderItem'

interface IRequest {
  order_id: string
  product_id: string
  amount: number
}

export class CreateOrderItemService {
  constructor(private readonly prismaorm = PrismaORM) {}

  public async execute({ order_id, product_id, amount }: IRequest): Promise<IOrderItem> {
    const orderCheck = await this.prismaorm.order.findUnique({
      where: { id: order_id }
    })

    if (orderCheck === null) {
      throw new InternalAppError('Order not found')
    }

    const productCheck = await this.prismaorm.product.findUnique({
      where: { id: product_id }
    })

    if (productCheck === null) {
      throw new InternalAppError('Product not found')
    }

    const orderItem = await this.prismaorm.orderItem.create({
      data: {
        order_id,
        product_id,
        amount
      }
    })

    return orderItem
  }
}
