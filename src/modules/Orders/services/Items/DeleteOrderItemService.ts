import { PrismaORM } from '../../../../shared/database'
import { InternalAppError } from '../../../../shared/utils/errors/internalAppError'

interface IRequest {
  orderItem_id: string
}

export class DeleteOrderItemService {
  constructor(private readonly prismaorm = PrismaORM) {}

  public async execute({ orderItem_id }: IRequest): Promise<void> {
    const order = await this.prismaorm.orderItem.findUnique({
      where: { id: orderItem_id }
    })

    if (order === null) {
      throw new InternalAppError('Item not found')
    }

    await this.prismaorm.orderItem.delete({
      where: { id: orderItem_id }
    })
  }
}
