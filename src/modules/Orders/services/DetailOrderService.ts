import { PrismaORM } from '../../../shared/database'
import { IOrderItem } from '../models/IOrderItem'

interface IRequest {
  order_id: string
}
export class DetailOrderService {
  constructor(private readonly prismaorm = PrismaORM) {}

  public async execute({ order_id }: IRequest): Promise<IOrderItem[]> {
    const orders = await this.prismaorm.orderItem.findMany({
      where: {
        order_id: order_id
      },
      include: {
        order: true,
        product: true
      }
    })

    return orders
  }
}
