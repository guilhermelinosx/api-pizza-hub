import { PrismaORM } from '../../../shared/database'
import { InternalAppError } from '../../../shared/utils/errors/internalAppError'
import { IOrder } from '../models/IOrder'

export class ReadAllOrderService {
  constructor(private readonly prismaorm = PrismaORM) {}

  public async execute(): Promise<IOrder[]> {
    const orderCheck = await this.prismaorm.order.findMany({
      where: {
        draft: false,
        status: false
      },

      orderBy: {
        createdAt: 'desc'
      }
    })

    if (orderCheck === null) {
      throw new InternalAppError('Orders not found')
    }

    return orderCheck
  }
}
