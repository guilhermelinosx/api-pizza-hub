import { PrismaORM } from '../../../shared/database'
import { InternalAppError } from '../../../shared/utils/errors/internalAppError'
import { IOrder } from '../models/IOrder'

interface IRequest {
  id: string
}

export class FinishOrderService {
  constructor(private readonly prismaorm = PrismaORM) {}

  public async execute({ id }: IRequest): Promise<IOrder> {
    const orderCheck = await this.prismaorm.order.findUnique({
      where: { id }
    })
    if (orderCheck === null) {
      throw new InternalAppError('Order not found')
    }

    const order = await this.prismaorm.order.update({
      where: { id },
      data: {
        status: true
      }
    })

    return order
  }
}
