import { PrismaORM } from '../../../shared/database'
import { InternalAppError } from '../../../shared/utils/errors/internalAppError'
import { IOrder } from '../models/IOrder'

interface IRequest {
  table: number
}

export class CreateOrderService {
  constructor(private readonly prismaorm = PrismaORM) {}

  public async execute({ table }: IRequest): Promise<IOrder> {
    const orderCheck = await this.prismaorm.order.findFirst({
      where: {
        table
      }
    })
    if (orderCheck !== null) {
      throw new InternalAppError('Order already exists.')
    }

    const order = await this.prismaorm.order.create({
      data: {
        table
      }
    })

    return order
  }
}
