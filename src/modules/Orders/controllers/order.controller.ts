import { Request, Response } from 'express'
import { CreateOrderItemService } from '../services/Items/CreateOrderItemService'
import { CreateOrderService } from '../services/CreateOrderService'
import { DeleteOrderItemService } from '../services/Items/DeleteOrderItemService'
import { DeleteOrderService } from '../services/DeleteOrderService'
import { DetailOrderService } from '../services/DetailOrderService'
import { FinishOrderService } from '../services/FinishOrderService'
import { ReadAllOrderService } from '../services/ReadAllOrderService'
import { SendOrderService } from '../services/SendOrderService'

export class OrderController {
  public async readAll(_req: Request, res: Response): Promise<Response> {
    try {
      const readAllOrderService = new ReadAllOrderService()
      const order = await readAllOrderService.execute()
      return res.status(200).json(order)
    } catch (err) {
      return res.status(400).json(err as string)
    }
  }

  public async create(req: Request, res: Response): Promise<Response> {
    try {
      const { table } = req.body
      const createOrderService = new CreateOrderService()
      const order = await createOrderService.execute({ table })
      return res.status(201).json(order)
    } catch (err) {
      return res.status(400).json(err as string)
    }
  }

  public async send(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.params
      const sendOrderService = new SendOrderService()
      const order = await sendOrderService.execute({ id })
      return res.status(200).json(order)
    } catch (err) {
      return res.status(400).json(err as string)
    }
  }

  public async detail(req: Request, res: Response): Promise<Response> {
    try {
      const order_id = req.query.order_id as string
      const detailOrderService = new DetailOrderService()
      const order = await detailOrderService.execute({ order_id })
      return res.status(200).json(order)
    } catch (err) {
      return res.status(400).json(err as string)
    }
  }

  public async finish(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.params
      const finishOrderService = new FinishOrderService()
      const order = await finishOrderService.execute({ id })
      return res.status(200).json(order)
    } catch (err) {
      return res.status(400).json(err as string)
    }
  }

  public async delete(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.params
      const deleteOrderService = new DeleteOrderService()
      await deleteOrderService.execute({ id })
      return res.status(200).json({ message: 'Order removed successfully' })
    } catch (err) {
      return res.status(400).json(err as string)
    }
  }

  public async createOrderItem(req: Request, res: Response): Promise<Response> {
    try {
      const { order_id, product_id, amount } = req.body
      const createOrderItemService = new CreateOrderItemService()
      const orderItem = await createOrderItemService.execute({ order_id, product_id, amount })
      return res.status(201).json(orderItem)
    } catch (err) {
      return res.status(400).json(err as string)
    }
  }

  public async deleteOrdemItem(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.params
      const deleteOrderItemService = new DeleteOrderItemService()
      await deleteOrderItemService.execute({ orderItem_id: id })
      return res.status(200).json({ message: 'Order item removed successfully' })
    } catch (err) {
      return res.status(400).json(err as string)
    }
  }
}
