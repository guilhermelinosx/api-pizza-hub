import { IOrderItem } from './IOrderItem'

export interface IOrder {
  id: string
  table: number
  status: boolean
  draft: boolean
  createdAt?: Date
  updatedAt?: Date
  OrderItem?: IOrderItem[]
}
