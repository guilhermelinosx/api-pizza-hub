export interface IOrderItem {
  id: string
  amount: number
  order_id: string
  product_id: string
  createdAt?: Date
  updatedAt?: Date
}
