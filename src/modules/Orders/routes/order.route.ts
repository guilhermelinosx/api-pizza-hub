import { celebrate, Joi, Segments } from 'celebrate'
import { Router } from 'express'
import { isAuthenticated } from '../../../shared/utils/middlewares/isAuthenticated'
import { OrderController } from '../controllers/order.controller'

export const OrderRouter = Router()
const orderController = new OrderController()

OrderRouter.use(isAuthenticated)

OrderRouter.get('/', orderController.readAll)

OrderRouter.post(
  '/',
  celebrate({
    [Segments.BODY]: {
      table: Joi.number().required()
    }
  }),
  orderController.create
)

OrderRouter.delete(
  '/:id',
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().required()
    }
  }),
  orderController.delete
)

OrderRouter.get(
  '/detail',
  celebrate({
    [Segments.QUERY]: {
      id: Joi.string().required()
    }
  }),
  orderController.detail
)

OrderRouter.put(
  '/send/:id',
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().required()
    }
  }),
  orderController.send
)

OrderRouter.put(
  '/finish/:id',
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().required()
    }
  }),
  orderController.finish
)

/////////////////////////////////////////

OrderRouter.post(
  '/add',
  celebrate({
    [Segments.BODY]: {
      order_id: Joi.string().required(),
      product_id: Joi.string().required(),
      amount: Joi.number().required()
    }
  }),
  orderController.createOrderItem
)

OrderRouter.delete(
  '/remove/:id',
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().required()
    }
  }),
  orderController.deleteOrdemItem
)
