import { Request, Response } from 'express'
import { CreateCategoryService } from '../services/CreateCategoryService'
import { DeleteCategoryService } from '../services/DeleteCategoryService'
import { ReadAllCategoryService } from '../services/ReadAllCategoryService'
import { UpdateCategoryService } from '../services/UpdateCategoryService'

export class CategoryController {
  public async create(req: Request, res: Response): Promise<Response> {
    try {
      const { name } = req.body
      const createCategoryService = new CreateCategoryService()
      const categories = await createCategoryService.execute({ name })
      return res.status(201).json(categories)
    } catch (err) {
      return res.status(400).json(err as string)
    }
  }

  public async readAll(_req: Request, res: Response): Promise<Response> {
    try {
      const readAllCategoryService = new ReadAllCategoryService()
      const categories = await readAllCategoryService.execute()
      return res.status(200).json(categories)
    } catch (err) {
      return res.status(400).json(err as string)
    }
  }

  public async update(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.params
      const { name } = req.body
      const updateCategoryService = new UpdateCategoryService()
      const categories = await updateCategoryService.execute({ id, name })
      return res.status(200).json(categories)
    } catch (err) {
      return res.status(400).json(err as string)
    }
  }

  public async delete(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.params
      const deleteCategoryService = new DeleteCategoryService()
      await deleteCategoryService.execute({ id })
      return res.status(200).json({ message: 'Category deleted successfully.', code: 200 })
    } catch (err) {
      return res.status(400).json(err as string)
    }
  }
}
