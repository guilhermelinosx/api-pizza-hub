import { PrismaORM } from '../../../shared/database'
import { InternalAppError } from '../../../shared/utils/errors/internalAppError'
import { ICategory } from '../models/ICategory'

interface IRequest {
  name: string
}

export class CreateCategoryService {
  constructor(private readonly prismaorm = PrismaORM) {}

  public async execute({ name }: IRequest): Promise<ICategory> {
    try {
      const categoryCheck = await this.prismaorm.category.findFirst({
        where: {
          name
        }
      })

      if (categoryCheck !== null) {
        throw new InternalAppError('Category already exists.')
      }

      const category = await this.prismaorm.category.create({
        data: {
          name
        }
      })

      return category
    } catch (err: unknown) {
      throw new InternalAppError(err as string)
    }
  }
}
