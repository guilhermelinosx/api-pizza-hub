import { PrismaORM } from '../../../shared/database'
import { InternalAppError } from '../../../shared/utils/errors/internalAppError'
import { ICategory } from '../models/ICategory'

interface IRequest {
  id: string
  name: string
}

export class UpdateCategoryService {
  constructor(private readonly prismaorm = PrismaORM) {}

  public async execute({ id, name }: IRequest): Promise<ICategory> {
    try {
      const categoryCheckID = await this.prismaorm.category.findUnique({
        where: {
          id
        }
      })

      if (categoryCheckID === null) {
        throw new Error('Category not found')
      }

      const categoryCheckName = await this.prismaorm.category.findFirst({
        where: {
          name
        }
      })

      if (categoryCheckName?.name === name) {
        throw new InternalAppError('There is already one category with this name.')
      }

      const category = await this.prismaorm.category.update({
        where: {
          id
        },
        data: {
          name
        }
      })

      return category
    } catch (err: unknown) {
      throw new InternalAppError(err as string)
    }
  }
}
