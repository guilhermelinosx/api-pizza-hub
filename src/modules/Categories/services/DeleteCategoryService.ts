import { PrismaORM } from '../../../shared/database'
import { InternalAppError } from '../../../shared/utils/errors/internalAppError'

interface IRequest {
  id: string
}

export class DeleteCategoryService {
  constructor(private readonly prismaorm = PrismaORM) {}

  public async execute({ id }: IRequest): Promise<void> {
    try {
      const categoryCheck = await this.prismaorm.category.findUnique({
        where: { id }
      })

      if (categoryCheck === null) {
        throw new InternalAppError('Category not found')
      }

      await this.prismaorm.category.delete({
        where: { id }
      })
    } catch (err: unknown) {
      throw new InternalAppError(err as string)
    }
  }
}
