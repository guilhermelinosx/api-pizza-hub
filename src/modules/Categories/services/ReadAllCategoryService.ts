import { PrismaORM } from '../../../shared/database'
import { InternalAppError } from '../../../shared/utils/errors/internalAppError'
import { ICategory } from '../models/ICategory'

export class ReadAllCategoryService {
  constructor(private readonly prismaorm = PrismaORM) {}

  public async execute(): Promise<ICategory[]> {
    try {
      const categoryCheck = await this.prismaorm.category.findMany()

      if (categoryCheck === null) {
        throw new InternalAppError('Categories not found')
      }

      return categoryCheck
    } catch (err: unknown) {
      throw new InternalAppError(err as string)
    }
  }
}
