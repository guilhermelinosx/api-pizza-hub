import { celebrate, Joi, Segments } from 'celebrate'
import { Router } from 'express'
import { isAuthenticated } from '../../../shared/utils/middlewares/isAuthenticated'
import { ProductController } from '../../Products/controllers/product.controller'
import { CategoryController } from '../controllers/category.controller'

export const CategoryRouter = Router()
const categoryController = new CategoryController()
const productController = new ProductController()

CategoryRouter.use(isAuthenticated)

CategoryRouter.get('/', categoryController.readAll)

CategoryRouter.post(
  '/',
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required()
    }
  }),
  categoryController.create
)

CategoryRouter.put(
  '/:id',
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required()
    },
    [Segments.PARAMS]: {
      id: Joi.string().uuid().required()
    }
  }),
  categoryController.update
)

CategoryRouter.delete(
  '/:id',
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().uuid().required()
    }
  }),
  categoryController.delete
)

CategoryRouter.get(
  '/products',
  celebrate({
    [Segments.QUERY]: {
      category_id: Joi.string().required()
    }
  }),
  productController.readbycategory
)
