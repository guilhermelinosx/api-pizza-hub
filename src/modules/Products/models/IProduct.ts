import { IOrderItem } from '../../Orders/models/IOrderItem'

export interface IProduct {
  id: string
  name: string
  price: number
  description: string
  createdAt?: Date
  updatedAt?: Date
  category_id: string
  OrderItem?: IOrderItem[]
}
