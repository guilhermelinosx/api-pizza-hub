import { PrismaORM } from '../../../shared/database'
import { InternalAppError } from '../../../shared/utils/errors/internalAppError'
import { IProduct } from '../models/IProduct'

interface IRequest {
  name: string
  description: string
  price: number
  category_id: string
}

export class CreateProductService {
  constructor(private readonly prismaorm = PrismaORM) {}

  public async execute({ name, description, price, category_id }: IRequest): Promise<IProduct> {
    const productCheck = await this.prismaorm.product.findFirst({
      where: {
        name
      }
    })

    if (productCheck != null) {
      throw new InternalAppError('Product already exists.')
    }

    const product = await this.prismaorm.product.create({
      data: {
        name,
        description,
        price,
        category_id
      }
    })

    return product
  }
}
