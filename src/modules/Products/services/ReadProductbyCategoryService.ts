import { PrismaORM } from '../../../shared/database'
import { InternalAppError } from '../../../shared/utils/errors/internalAppError'
import { IProduct } from '../models/IProduct'

interface IRequest {
  category_id: string
}

export class ReadProductbyCategoryService {
  constructor(private readonly prismaorm = PrismaORM) {}

  public async execute({ category_id }: IRequest): Promise<IProduct[]> {
    const products = await this.prismaorm.product.findMany({
      where: {
        category_id
      }
    })

    if (products === null) {
      throw new InternalAppError('Products not found')
    }

    return products
  }
}
