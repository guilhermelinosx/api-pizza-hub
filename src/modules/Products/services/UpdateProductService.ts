import { PrismaORM } from '../../../shared/database'
import { InternalAppError } from '../../../shared/utils/errors/internalAppError'
import { IProduct } from '../models/IProduct'

interface IRequest {
  id: string
  name: string
  price: number
  description: string
  category_id: string
}

export class UpdateProductService {
  constructor(private readonly prismaorm = PrismaORM) {}

  public async execute({ id, name, price, description, category_id }: IRequest): Promise<IProduct> {
    const productCheckID = await this.prismaorm.product.findUnique({
      where: { id }
    })

    if (productCheckID === null) {
      throw new InternalAppError('Product not found')
    }

    const productCheckName = await this.prismaorm.product.findFirst({
      where: {
        name
      }
    })

    if (productCheckName?.name === name) {
      throw new InternalAppError('There is already one product with this name.')
    }

    const product = await this.prismaorm.product.update({
      where: {
        id
      },
      data: {
        name,
        price,
        description,
        category_id
      }
    })

    return product
  }
}
