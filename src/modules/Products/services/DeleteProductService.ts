import { PrismaORM } from '../../../shared/database'
import { InternalAppError } from '../../../shared/utils/errors/internalAppError'

interface IRequest {
  id: string
}

export class DeleteProductService {
  constructor(private readonly prismaorm = PrismaORM) {}

  public async execute({ id }: IRequest): Promise<void> {
    const productCheck = await this.prismaorm.product.findUnique({
      where: { id }
    })

    if (productCheck === null) {
      throw new InternalAppError('Product not found')
    }

    await this.prismaorm.product.delete({
      where: { id }
    })
  }
}
