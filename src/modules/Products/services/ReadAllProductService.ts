import { PrismaORM } from '../../../shared/database'
import { InternalAppError } from '../../../shared/utils/errors/internalAppError'
import { IProduct } from '../models/IProduct'

export class ReadAllProductService {
  constructor(private readonly prismaorm = PrismaORM) {}

  public async execute(): Promise<IProduct[]> {
    const productCheck = await this.prismaorm.product.findMany()

    if (productCheck === null) {
      throw new InternalAppError('Products with this category not found')
    }

    return productCheck
  }
}
