import { Request, Response } from 'express'
import { CreateProductService } from '../services/CreateProductService'
import { DeleteProductService } from '../services/DeleteProductService'
import { ReadAllProductService } from '../services/ReadAllProductService'
import { ReadProductbyCategoryService } from '../services/ReadProductbyCategoryService'
import { UpdateProductService } from '../services/UpdateProductService'

export class ProductController {
  public async readAll(_req: Request, res: Response): Promise<Response> {
    try {
      const readAllProductService = new ReadAllProductService()
      const products = await readAllProductService.execute()
      return res.status(200).json(products)
    } catch (err) {
      return res.status(400).json(err as string)
    }
  }

  public async create(req: Request, res: Response): Promise<Response> {
    try {
      const { name, description, price, category_id } = req.body
      const createProductService = new CreateProductService()
      const products = await createProductService.execute({ name, description, price, category_id })
      return res.status(201).json(products)
    } catch (err) {
      return res.status(400).json(err as string)
    }
  }

  public async update(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.params
      const { name, description, price, category_id } = req.body
      const updateProductService = new UpdateProductService()
      const products = await updateProductService.execute({
        id,
        name,
        description,
        price,
        category_id
      })
      return res.status(200).json(products)
    } catch (err) {
      return res.status(400).json(err as string)
    }
  }

  public async delete(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.params
      const deleteProductService = new DeleteProductService()
      await deleteProductService.execute({ id })
      return res.status(200).json({ message: 'Product deleted successfully.', code: 200 })
    } catch (err) {
      return res.status(400).json(err as string)
    }
  }

  public async readbycategory(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.params
      const readProductByCategoryService = new ReadProductbyCategoryService()
      const products = await readProductByCategoryService.execute({ category_id: id })
      return res.status(200).json(products)
    } catch (err) {
      return res.status(400).json(err)
    }
  }
}
