import { celebrate, Joi, Segments } from 'celebrate'
import { Router } from 'express'
import { isAuthenticated } from '../../../shared/utils/middlewares/isAuthenticated'
import { ProductController } from '../controllers/product.controller'

export const ProductRouter = Router()
const productController = new ProductController()

ProductRouter.use(isAuthenticated)

ProductRouter.get('/', productController.readAll)

ProductRouter.post(
  '/',
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required(),
      description: Joi.string().required(),
      price: Joi.number().required(),
      category_id: Joi.string().required()
    }
  }),
  productController.create
)

ProductRouter.put(
  '/:id',
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().uuid().required()
    },
    [Segments.BODY]: {
      name: Joi.string().optional(),
      description: Joi.string().optional(),
      price: Joi.number().optional(),
      category_id: Joi.string().optional()
    }
  }),
  productController.update
)

ProductRouter.delete(
  '/:id',
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().uuid().required()
    }
  }),
  productController.delete
)
