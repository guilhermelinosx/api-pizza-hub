export class InternalAppError {
  constructor(public message: string, protected code: number = 400) {
    this.message = message
    this.code = code
  }
}
