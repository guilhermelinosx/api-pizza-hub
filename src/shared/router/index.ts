import { Router } from 'express'
import { CategoryRouter } from '../../modules/Categories/routes/category.route'
import { OrderRouter } from '../../modules/Orders/routes/order.route'
import { ProductRouter } from '../../modules/Products/routes/product.route'
import { UserRouter } from '../../modules/Users/routes/user.route'

export const router = Router()

router.use('/api', UserRouter)
router.use('/api/categories', CategoryRouter)
router.use('/api/products', ProductRouter)
router.use('/api/orders', OrderRouter)

router.get('/', (_req, res) => {
  res.send('API Pizza Hub is running')
})
