import { app } from './app'

try {
  app.listen(process.env.PORT || 3001, () => {
    console.info(`Server listening on: http://localhost:${process.env.PORT as string} 🚀`)
  })
} catch (err: unknown) {
  console.info(err as string)
}
