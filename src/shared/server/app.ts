import 'dotenv/config'
import 'express-async-errors'
import cors from 'cors'
import express, { NextFunction, Request, Response } from 'express'
import { errors } from 'celebrate'
import { router } from '../router'
import { InternalAppError } from '../utils/errors/internalAppError'

export const app = express()

app.use(express.json())
app.use(errors())
app.use(cors())

app.use(router)

app.use((err: Error, _req: Request, res: Response, _next: NextFunction) => {
  if (err instanceof InternalAppError) {
    return res.status(400).json({
      status: 'Error',
      message: err.message
    })
  }

  return res.status(500).json({
    status: 'Error',
    message: 'Internal server error'
  })
})
